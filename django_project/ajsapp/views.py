# coding: utf-8
from datetime import datetime

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from ajsapp.models import Noticia, Liturgia, Evento, Musica, Album, Intencao, Cifra, Categoria
from ajsapp.serializers import NoticiaSerializer, LiturgiaSerializer, EventoSerializer, MusicaSerializer, AlbumSerializer, IntencaoSerializer, CifraSerializer, CategoriaSerializer


def index(request):
    return HttpResponse("AJS - Pastoral Juvenil.")


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        """
        """
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json; charset=utf-8'
        super(JSONResponse, self).__init__(content, **kwargs)


def noticia_list(request):
    """
    Lista as últimas 20 notícias cadastradas

    **Context**

    ``/api/noticias/``

    :rtype : object

    :param request: 
         Uma instância do :model:`ajsapp.Noticia`.
    """

    if request.method == 'GET':
        noticias = Noticia.objects.all().order_by('-pk')[:10]
        serializer = NoticiaSerializer(noticias, many=True)
        return JSONResponse(serializer.data)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = NoticiaSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def noticia_list_old(request, pk):
    """
    Lista as 20 notícias cadastradas onde o id é menor que o informado no parametro (Noticias antigas).
    **Context**
    ``/api/noticias/old/<id>``
         Uma instância do :model:`ajsapp.Noticia`.
    """
    if request.method == 'GET':
        noticias = Noticia.objects.filter(id__lt=pk)[:10]
        serializer = NoticiaSerializer(noticias, many=True)
        return JSONResponse(serializer.data)


@csrf_exempt
def noticia_list_new(request, pk):
    """
    Lista as 20 notícias cadastradas onde o id é maior que o informado no parametro (Noticias novas).
    **Context**

    ``/api/noticias/new/<id>``
         Uma instância do :model:`ajsapp.Noticia`.
    """
    if request.method == 'GET':
        noticias = Noticia.objects.filter(id__gt=pk)[:10]
        serializer = NoticiaSerializer(noticias, many=True)
        return JSONResponse(serializer.data)


@csrf_exempt
def noticia_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        noticia = Noticia.objects.get(pk=pk)
    except Noticia.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = NoticiaSerializer(noticia)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = NoticiaSerializer(noticia, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        noticia.delete()
        return HttpResponse(status=204)


@csrf_exempt
def liturgia_list(request):
    """
    List all code snippets, or create a new snippet.
    """

    if request.method == 'GET':
        liturgias = Liturgia.objects.all()
        serializer = LiturgiaSerializer(liturgias, many=True)
        return JSONResponse(serializer.data)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = LiturgiaSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def liturgia_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        liturgia = Liturgia.objects.get(pk=pk)
    except Liturgia.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = LiturgiaSerializer(liturgia)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = LiturgiaSerializer(liturgia, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        liturgia.delete()
        return HttpResponse(status=204)


@csrf_exempt
def liturgia_updater(request, pk):
    """
    Verifica se existem liturgias cadastradas com id maior que o parâmetro informado.

    Utilizado para verificar se o aplicativo que consome a API está com dados atualizados

    **URL**

    ``/api/liturgia/atualizador/<id>``

    **Context:**

    :model:`ajsapp.Liturgia`

    **View:**

    :view:`ajsapp.views.lturgia_updater`

    """

    if Liturgia.objects.filter(id__gt=pk):
        return HttpResponse('{"id": "True"}')

    else:
        return HttpResponse('{"id": "False"}')


@csrf_exempt
def evento_list(request):
    """
    Lista os 20 eventos cadastrados cuja data do evento é >= que a data do dia corrente.

    **Context**
    `/api/eventos/`

    """
    if request.method == 'GET':
        eventos = Evento.objects.filter(data_evento__gte=datetime.now()).order_by('data_evento')[:10]
        serializer = EventoSerializer(eventos, many=True)
        return JSONResponse(serializer.data)


    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = EventoSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def evento_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        evento = Evento.objects.get(pk=pk)
    except Evento.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = EventoSerializer(evento)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = EventoSerializer(evento, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        evento.delete()
        return HttpResponse(status=204)


@csrf_exempt
def musica_list(request):
    """
    List all code snippets, or create a new snippet.
    """

    if request.method == 'GET':
        musicas = Musica.objects.all()
        serializer = MusicaSerializer(musicas, many=True)
        return JSONResponse(serializer.data)


    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = MusicaSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def musica_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        musica = Musica.objects.get(pk=pk)
    except Musica.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = MusicaSerializer(musica)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = MusicaSerializer(musica, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        musica.delete()
        return HttpResponse(status=204)


@csrf_exempt
def album_list(request):
    """
    List all code snippets, or create a new snippet.
    """

    if request.method == 'GET':
        albuns = Album.objects.filter(publicado=True)
        serializer = AlbumSerializer(albuns, many=True)
        return JSONResponse(serializer.data)


    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = AlbumSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def album_musica_list(request, pk):
    """
    List all code snippets, or create a new snippet.
    """

    if request.method == 'GET':
        musicas = Musica.objects.all().filter(album__id=pk)
        serializer = MusicaSerializer(musicas, many=True)
        return JSONResponse(serializer.data)


    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = MusicaSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def album_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        album = Album.objects.get(pk=pk)
    except album.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = AlbumSerializer(album)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = AlbumSerializer(album, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        album.delete()
        return HttpResponse(status=204)


@csrf_exempt
def intencao_list(request):
    """
    Lista/Insere Intenções (Orações)

    Para Inserir uma nova intenção basta preencher todos os campos, exceto ID.

    Para atualizar um campo, basta carregar o objeto, alterar o campo desejado e enviar o objeto novamente.

    **URL**

    ``/api/liturgia/atualizador/<id>``

    **Context:**

    :model:`ajsapp.Liturgia`

    **View:**

    :view:`ajsapp.views.lturgia_updater`

    """
    if request.method == 'GET':
        intencoes = Intencao.objects.all().order_by('-pk')[:30]
        serializer = IntencaoSerializer(intencoes, many=True)
        return JSONResponse(serializer.data)


    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = IntencaoSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def intencao_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        intencao = Intencao.objects.get(pk=pk)
    except Intencao.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = IntencaoSerializer(intencao)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = IntencaoSerializer(intencao, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        intencao.delete()
        return HttpResponse(status=204)


@csrf_exempt
def cifra_list(request):
    """
    List all code snippets, or create a new snippet.
    """

    if request.method == 'GET':
        cifras = Cifra.objects.all()
        serializer = CifraSerializer(cifras, many=True)
        return JSONResponse(serializer.data)


    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CifraSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)



@csrf_exempt
def cifra_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        cifra = Cifra.objects.get(pk=pk)
    except Cifra.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = CifraSerializer(cifra)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = CifraSerializer(cifra, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        cifra.delete()
        return HttpResponse(status=204)


@csrf_exempt
def cifra_list_categoria(request, pk):
    """
    List all code snippets, or create a new snippet.
    """

    if request.method == 'GET':
        cifras = Cifra.objects.all().filter(categoria__id=pk)
        serializer = CategoriaSerializer(cifras, many=True)
        return JSONResponse(serializer.data)


    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CategoriaSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def categoria_list(request):
    """
    List all code snippets, or create a new snippet.
    """

    if request.method == 'GET':
        categorias = Categoria.objects.filter(publicado=True)
        serializer = CategoriaSerializer(categorias, many=True)
        return JSONResponse(serializer.data)


    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CategoriaSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def categoria_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        categoria = Categoria.objects.get(pk=pk)
    except categoria.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = CategoriaSerializer(categoria)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = CategoriaSerializer(categoria, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        categoria.delete()
        return HttpResponse(status=204)