from ajsapp.models import Noticia, Liturgia, Evento, Musica, Album, Intencao, Cifra
from rest_framework import serializers


class NoticiaSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    titulo = serializers.CharField(required=False, allow_blank=True, max_length=300)
    descricao = serializers.CharField(required=False,allow_blank=True)
    thumbnail = serializers.CharField(required=False)
    data_criacao = serializers.DateField(required=False)
    publicado = serializers.BooleanField(required=False)

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Noticia.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.titulo = validated_data.get('titulo', instance.titulo)
        instance.descricao = validated_data.get('descricao', instance.descricao)
        instance.thumbnail = validated_data.get('thumbnail', instance.thumbnail)
        instance.data_criacao = validated_data.get('data_criacao', instance.data_criacao)
        instance.publicado = validated_data.get('publicado', instance.publicado)

        instance.save()
        return instance


class LiturgiaSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    data_liturgia = serializers.DateField(required=False)
    titulo_liturgia = serializers.CharField(required=False)
    titulo_leitura = serializers.CharField(required=False)
    texto_leitura = serializers.CharField(required=False)
    titulo_leitura2 = serializers.CharField(required=False)
    texto_leitura2 = serializers.CharField(required=False)
    titulo_salmo = serializers.CharField(required=False)
    texto_salmo = serializers.CharField(required=False)
    titulo_evangelho = serializers.CharField(required=False)
    texto_evangelho = serializers.CharField(required=False)
    reflexao = serializers.CharField(required=False)


    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Liturgia.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.data_liturgia = validated_data.get('data_liturgia', instance.data_liturgia)
        instance.titulo_liturgia = validated_data.get('titulo_liturgia', instance.titulo_liturgia)
        instance.data_liturgia = validated_data.get('data_liturgia', instance.data_liturgia)
        instance.titulo_leitura = validated_data.get('titulo_leitura', instance.titulo_leitura)
        instance.texto_leitura = validated_data.get('texto_leitura', instance.texto_leitura)
        instance.titulo_leitura2 = validated_data.get('titulo_leitura2', instance.titulo_leitura2)
        nstance.texto_leitura2 = validated_data.get('texto_leitura2', instance.texto_leitura2)
        nstance.titulo_salmo = validated_data.get('titulo_salmo2', instance.titulo_salmo)
        nstance.texto_salmo = validated_data.get('texto_salmo', instance.texto_salmo)
        nstance.titulo_evangelhoo = validated_data.get('titulo_evangelhoo', instance.titulo_evangelhoo)
        nstance.texto_evangelho = validated_data.get('texto_evangelho', instance.texto_evangelho)
        nstance.reflexao = validated_data.get('reflexao', instance.reflexao)

        instance.save()
        return instance


class EventoSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    titulo = serializers.CharField(required=False, allow_blank=True, max_length=300)
    descricao = serializers.CharField(required=False,allow_blank=True)
    thumbnail = serializers.CharField(required=False)
    data_evento = serializers.DateTimeField(required=False)
    data_criacao = serializers.DateField(required=False)
    publicado = serializers.BooleanField(required=False)

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Evento.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.titulo = validated_data.get('titulo', instance.titulo)
        instance.descricao = validated_data.get('descricao', instance.descricao)
        instance.thumbnail = validated_data.get('thumbnail', instance.thumbnail)
        instance.data_evento = validated_data.get('data_evento', instance.data_evento)
        instance.data_criacao = validated_data.get('data_criacao', instance.data_criacao)
        instance.publicado = validated_data.get('publicado', instance.publicado)

        instance.save()
        return instance

class MusicaSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    titulo = serializers.CharField(required=False, allow_blank=True, max_length=300)
    descricao = serializers.CharField(required=False, allow_blank=True)
    arquivo_musica = serializers.CharField(required=False)
    data_criacao = serializers.DateField(required=False)
    publicado = serializers.BooleanField(required=False)
    album = serializers.CharField(required=False)

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Musica.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.titulo = validated_data.get('titulo', instance.titulo)
        instance.descricao = validated_data.get('descricao', instance.descricao)
        instance.arquivo_musica = validated_data.get('arquivo_musica', instance.arquivo_musica)
        instance.data_criacao = validated_data.get('data_criacao', instance.data_criacao)
        instance.publicado = validated_data.get('publicado', instance.publicado)

        instance.save()
        return instance


class AlbumSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    titulo = serializers.CharField(required=False, allow_blank=True, max_length=300)
    descricao = serializers.CharField(required=False, allow_blank=True)
    data_criacao = serializers.DateField(required=False)
    publicado = serializers.BooleanField(required=False)

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Album.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.titulo = validated_data.get('titulo', instance.titulo)
        instance.descricao = validated_data.get('descricao', instance.descricao)
        instance.data_criacao = validated_data.get('data_criacao', instance.data_criacao)
        instance.publicado = validated_data.get('publicado', instance.publicado)

        instance.save()
        return instance


class IntencaoSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    nome = serializers.CharField(required=False, allow_blank=True, max_length=300)
    intencao = serializers.CharField(required=False, allow_blank=True)
    rezas = serializers.IntegerField(default=0)
    data_criacao = serializers.DateField(required=False)
    publicado = serializers.BooleanField(required=False, default=False)

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Intencao.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.nome = validated_data.get('nome', instance.nome)
        instance.intencao = validated_data.get('intencao', instance.intencao)
        instance.rezas = validated_data.get('rezas', instance.rezas)
        instance.data_criacao = validated_data.get('data_criacao', instance.data_criacao)
        instance.publicado = validated_data.get('publicado', instance.publicado)

        instance.save()
        return instance


class CifraSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    titulo = serializers.CharField(required=False, allow_blank=True, max_length=300)
    descricao = serializers.CharField(required=False, allow_blank=True)
    data_criacao = serializers.DateField(required=False)
    publicado = serializers.BooleanField(required=False)
    categoria = serializers.CharField(required=False)

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Musica.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.titulo = validated_data.get('titulo', instance.titulo)
        instance.descricao = validated_data.get('descricao', instance.descricao)
        instance.data_criacao = validated_data.get('data_criacao', instance.data_criacao)
        instance.publicado = validated_data.get('publicado', instance.publicado)

        instance.save()
        return instance


class CategoriaSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    titulo = serializers.CharField(required=False, allow_blank=True, max_length=300)
    descricao = serializers.CharField(required=False, allow_blank=True)
    data_criacao = serializers.DateField(required=False)
    publicado = serializers.BooleanField(required=False)

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Album.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.titulo = validated_data.get('titulo', instance.titulo)
        instance.descricao = validated_data.get('descricao', instance.descricao)
        instance.data_criacao = validated_data.get('data_criacao', instance.data_criacao)
        instance.publicado = validated_data.get('publicado', instance.publicado)

        instance.save()
        return instance