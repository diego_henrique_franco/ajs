from django.conf.urls import url, patterns
from ajsapp import views

urlpatterns = [
               url(r'^api/noticias/$', views.noticia_list),
               url(r'^api/noticias/old/(?P<pk>[0-9]+)/$', views.noticia_list_old),
               url(r'^api/noticias/new/(?P<pk>[0-9]+)/$', views.noticia_list_new),
               url(r'^api/noticias/(?P<pk>[0-9]+)/$', views.noticia_detail),

               url(r'^api/liturgias/$', views.liturgia_list),
               url(r'^api/liturgias/atualizador/(?P<pk>[0-9]+)/$', views.liturgia_updater),
               url(r'^api/liturgias/(?P<pk>[0-9]+)/$', views.liturgia_detail),

               url(r'^api/eventos/$', views.evento_list),
               url(r'^api/eventos/(?P<pk>[0-9]+)/$', views.evento_detail),

               url(r'^api/albuns/$', views.album_list),
               url(r'^api/albuns/(?P<pk>[0-9]+)/$', views.album_detail),

               url(r'^api/categorias/$', views.categoria_list),
               url(r'^api/categorias/(?P<pk>[0-9]+)/$', views.categoria_detail),

               url(r'^api/musicas/albuns/(?P<pk>[0-9]+)/$', views.album_musica_list),
               url(r'^api/cifras/categorias/(?P<pk>[0-9]+)/$', views.cifra_list_categoria),

               url(r'^api/musicas/$', views.musica_list),
               url(r'^api/musicas/(?P<pk>[0-9]+)/$', views.musica_detail),

               url(r'^api/cifras/$', views.cifra_list),
               url(r'^api/cifras/(?P<pk>[0-9]+)/$', views.cifra_detail),

               url(r'^api/intencoes/$', views.intencao_list),
               url(r'^api/intencoes/(?P<pk>[0-9]+)/$', views.intencao_detail), ]