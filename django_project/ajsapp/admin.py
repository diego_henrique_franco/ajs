from django.contrib import admin
from models import Noticia, Evento, Liturgia, Musica, Intencao, Album, Cifra, Categoria

# Register your models here.

admin.site.register(Noticia)
admin.site.register(Evento)
admin.site.register(Liturgia)
admin.site.register(Album)
admin.site.register(Musica)
admin.site.register(Intencao)
admin.site.register(Cifra)
admin.site.register(Categoria)