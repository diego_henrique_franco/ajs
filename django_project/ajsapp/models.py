# coding: utf-8
from django.db import models


class Pessoa(models.Model):
    nome = models.CharField(max_length=60)
    email = models.EmailField(max_length=60)
    data_nascimento = models.DateField()
    cpf = models.CharField(max_length=11)
    profissao = models.CharField(max_length=60)
    endereco = models.CharField(max_length=255, null=True)
    numero = models.CharField(max_length=10, null=True)
    bairro = models.CharField(max_length=60, null=True)
    complemento = models.CharField(max_length=100, blank=True, null=True)
    cidade = models.CharField(max_length=60, null=True)
    estado = models.ForeignKey('Estado', null=True)
    cep = models.CharField(max_length=8, null=True)
    telefone = models.CharField(max_length=10, null=True, blank=True)
    celular = models.CharField(max_length=10, null=True)

    def __unicode__(self):
        return "%s" % (self.nome)


class Estado(models.Model):
    uf = models.CharField(max_length=2)
    nome = models.CharField(max_length=60)

    def __unicode__(self):
        return "%s" % (self.uf)


class Noticia(models.Model):
    titulo = models.CharField(max_length=300)
    descricao = models.TextField(blank=False)
    thumbnail = models.FileField(upload_to='ajs/noticias/%Y/%m/')
    data_criacao = models.DateField(null=True, auto_now_add=True)
    publicado = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s" % (self.titulo)


class Liturgia(models.Model):
    data_liturgia = models.DateField()
    titulo_liturgia = models.CharField(max_length=300)
    titulo_leitura = models.CharField(max_length=300)
    texto_leitura = models.TextField()
    titulo_salmo = models.CharField(max_length=300)
    texto_salmo = models.TextField()
    titulo_leitura2 = models.CharField(max_length=300, blank=True)
    texto_leitura2 = models.TextField(blank=True)
    titulo_evangelho = models.CharField(max_length=300)
    texto_evangelho = models.TextField()
    reflexao = models.TextField(null=True)

    def __unicode__(self):
        return "%s" % (self.titulo_liturgia)


class Evento(models.Model):
    titulo = models.CharField(max_length=300)
    descricao = models.TextField()
    thumbnail = models.FileField(upload_to='ajs/eventos/%Y/%m/')
    data_evento = models.DateTimeField(null=True)
    data_criacao = models.DateField(null=True, auto_now_add=True)
    publicado = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s" % self.titulo


class Musica(models.Model):
    titulo = models.CharField(max_length=100)
    descricao = models.TextField()
    arquivo_musica = models.FileField(upload_to='ajs/musicas/%Y/%m/')
    data_criacao = models.DateField(null=True, auto_now_add=True)
    publicado = models.BooleanField(default=True)
    album = models.ForeignKey('Album', null=True)

    def __unicode__(self):
        return "%s" % self.titulo


class Album(models.Model):
    titulo = models.CharField(max_length=100)
    descricao = models.TextField()
    data_criacao = models.DateField(null=True, auto_now_add=True)
    publicado = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s" % self.titulo


class Intencao(models.Model):
    nome = models.CharField(max_length=100)
    intencao = models.TextField()
    rezas = models.IntegerField(default=0)
    data_criacao = models.DateField(null=True, auto_now_add=True)
    publicado = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s - %s" % (self.nome, self.intencao)


class Categoria(models.Model):
    titulo = models.CharField(max_length=100)
    descricao = models.TextField()
    data_criacao = models.DateField(null=True, auto_now_add=True)
    publicado = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s" % self.titulo


class Cifra(models.Model):
    titulo = models.CharField(max_length=100)
    descricao = models.TextField()
    data_criacao = models.DateField(null=True, auto_now_add=True)
    publicado = models.BooleanField(default=True)
    categoria = models.ForeignKey('Categoria', null=True)

    def __unicode__(self):
        return "%s" % self.titulo
