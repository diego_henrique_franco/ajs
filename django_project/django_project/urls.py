from django.conf.urls import patterns, include, url
from ajsapp import views
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^api/', include('ajsapp.urls', namespace='API', app_name='ajsapp')),
                       url(r'^', include('ajsapp.urls')),
                       url(r'^$', views.index, name='index'),

                       url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
                       url(r'^admin/', include(admin.site.urls)),

                       ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
