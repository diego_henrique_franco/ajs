Django==1.8.6
djangorestframework==3.3.1
docutils==0.12
psycopg2==2.4.5
Pygments==2.0.2
wheel==0.24.0
